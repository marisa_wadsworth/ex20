﻿using System;

namespace ex20Part1
{
    class program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            /// Task A ///

            int num1; 
            int num2;

            Console.WriteLine("Please type in your first number");
            num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("PLease type in your second number");
            num2 = int.Parse(Console.ReadLine());
            
            //If Statement
            if(num1 > num2)
            {
                Console.WriteLine("Your first number is higher than your second number");
            }

            //Else If Statement
            else if(num2 > num1)
            {
                Console.WriteLine("Your second number is higher than your first number");
            }
            
            //Else Statemente
            else
            {
                Console.WriteLine("Both your numbers are the same");
            }

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");


            /// Task B ///

            string userPassword;

            Console.WriteLine("Please enter your password.");
            Console.WriteLine("This needs to be at least 8 characters long");
            userPassword = Console.ReadLine();
            Console.WriteLine();

            if(userPassword.Length < 8)
            {
                Console.WriteLine("Your password is too short. Please try again.");
            }

            else
            {
                Console.WriteLine("Thank you, your password is successful");
            }

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");

            /// Part C ///

            string userOldPassword;
            string userNewPassword;

            Console.WriteLine("Please change your password");
            Console.WriteLine("Please enter your old password");
            userOldPassword = Console.ReadLine();
            userPassword = userOldPassword;

            Console.WriteLine("");
            Console.WriteLine("Please type in your new password");
            userNewPassword = Console.ReadLine();
            userPassword = userPassword.Replace(userOldPassword, userNewPassword);
            Console.WriteLine("Thank you, your password has been successfully changed");


            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}

